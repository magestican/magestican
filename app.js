$(document).ready(function() {

  $('.tabular.menu .item').tab();


  window.initializeThings = function() {


      $('.help')
        .popup({
          inline: true,
          hoverable: true,
          position: 'right',
          delay: {
            show: 300,
            hide: 800
          }
        });


      $("form").submit(function() {
        return false;
      });

      $('.ui.checkbox')
        .checkbox();
      $(".ui.radio[value='mel']").click()

      $('.ui.dropdown')
        .dropdown();


      $('.time.ui.dropdown')
        .dropdown({
          allowAdditions: true
        });


      $('#acceptImportant')
        .popup({
          position: 'right center',
          target: '.info.admin.circle.icon',
          title: 'Primary Administrator',
          content: 'This person will be able to rule over the universe, be sure of this decision.'
        })

      $('#videoQuality')
        .popup({
          position: 'right center',
          target: '.info.quality.circle.icon',
          title: 'Call Quality',
          content: 'Here you can select the maximum allowed quality for your video room.'
        })



      $('#callersAccess')
        .popup({
          position: 'bottom center',
          target: '.info.access.circle.icon',
          title: 'Caller Access',
          content: 'Callers can have different levels of access to different areas of the video room.'
        })

      $('#qualityOption')
        .popup({
          position: 'right center',
          target: '.info.hd.circle.icon',
          title: 'Call Quality',
          content: 'Only recomended for very fast internet connections.'
        })



      $("#mondayTrigger")
        .popup({
          popup: $('#mondayTimes'),
          on: 'click'
        });


    }
    //navigation hardcoded
  $("#zerofour").click();
  window.smartclick = function() {
    $("[smartclick]").each(function(i, o) {
      var customFunction = $(o).attr("smartclick");
      $(o).click(function() {
        console.log("asd")
        eval(customFunction)
      })
    })
  }
  window.smartclick();
  window.initializeThings();
})
